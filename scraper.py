# Imports
from bs4 import BeautifulSoup
from requests import get
import re
import pandas as pd
import numpy as np

# regex = r'(\d)\W*(\S* \S*)\W*([A-Za-z]*)(\d*)'

# Each row is for a different for each statistic
# stat type is the first entry in the row
# season comes from page it is on
# Construct Dictionary
class Scraper:
    def __init__(self, url, paths, fields, header):
        self.base_url = url
        self.paths = paths
        self.request_header = header
        self.fields = fields
        self.df = None

    def request_page(self, path=None):
        url = self.base_url + path
        response = get(url, headers=self.request_header)
        html_soup = BeautifulSoup(response.text, 'lxml')
        return html_soup

    # Specific to each website
    def parse_page(self, path=None):
        return np.empty((1, len(self.fields)))

    def parse_site(self):
        data = np.empty((1, len(self.fields)))
        for path in self.paths:
            parsed_page = self.parse_page(path)
            data = np.append(data, parsed_page, axis=0)
        data = np.delete(data, 0, 0)
        return pd.DataFrame(data, columns=self.fields)
